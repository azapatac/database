﻿namespace DataBase
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Net.Http;
    using System.Threading.Tasks;
    using DataBase.Entities;
    using DataBase.Models;
    using Newtonsoft.Json;
    using Realms;
    using Xamarin.Essentials;
    using Xamarin.Forms;

    public partial class MainPage : ContentPage
    {
        const string urlBase = "https://restcountries.eu/rest/v2/";
        private readonly HttpClient client = new HttpClient();
        private IList<CountryModel> countries;

        readonly Realm DataBase;

        public MainPage()
        {            
            DataBase = Realm.GetInstance();
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var countries = DataBase.All<CountryEntity>();

            List<CountryModel> data = new List<CountryModel>();

            foreach (var country in countries)
            {
                CountryModel model = new CountryModel
                {
                    Capital = country.Capital,
                    Code = country.Code,
                    Name = country.Name,
                    Region = country.Region,
                    Subregion = country.Subregion
                };

                data.Add(model);
            }

            CollectionView.ItemsSource = new ObservableCollection<CountryModel>(data);
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                await App.Current.MainPage.DisplayAlert("DataBase", "No Internet", "OK");
                return;
            }

            try
            {                
                indicator.IsRunning = true;
                CollectionView.ItemsSource = null;
                string content = await client.GetStringAsync(urlBase);
                List<CountryModel> data = JsonConvert.DeserializeObject<List<CountryModel>>(content);
                countries = new ObservableCollection<CountryModel>(data);
                CollectionView.ItemsSource = countries;
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("DataBase", ex.Message, "OK");
            }
            finally
            {
                indicator.IsRunning = false;
            }
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            if (await App.Current.MainPage.DisplayAlert("DataBase", "Do you wanna save countries?", "OK", "Cancel") == false)
            {
                return;
            }

            try
            {
                indicator.IsRunning = true;

                using (var trans = DataBase.BeginWrite())
                {
                    DataBase.RemoveAll<CountryEntity>();
                    trans.Commit();
                }

                var countries = (IList<CountryModel>)CollectionView.ItemsSource;

                await DataBase.WriteAsync(tempRealm =>
                {
                    foreach (var country in countries)
                    {
                        CountryEntity entity = new CountryEntity
                        {
                            Capital = country.Capital,
                            Code = country.Code,
                            Name = country.Name,
                            Region = country.Region,
                            Subregion = country.Subregion
                        };

                        tempRealm.Add(entity);
                    };
                });
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("DataBase", ex.Message, "OK");
            }
            finally
            {
                indicator.IsRunning = false;
            }
        }
    }
}