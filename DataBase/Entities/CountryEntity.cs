﻿namespace DataBase.Entities
{
    using Realms;

    public class CountryEntity : RealmObject
    {
        [PrimaryKey]
        public string Code { get; set; }
        public string Capital { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }
        public string Subregion { get; set; }
    }
}